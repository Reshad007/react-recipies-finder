import React from 'react';

import ContentLoader from 'react-content-loader';

export const MyLoader = (props) => (
	<ContentLoader height={475} width={800} speed={0.8} primaryColor="#f3f3f3" secondaryColor="#ecebeb" {...props}>
		<rect x="43.14" y="30.61" rx="0" ry="0" width="0" height="0" />
		<circle cx="429.72" cy="158.61" r="1" />
		<rect x="88.31" y="52.61" rx="0" ry="0" width="96.78" height="22" />
		<rect x="85.72" y="97.61" rx="0" ry="0" width="310.2" height="206" />
		<rect x="401.63" y="50.61" rx="0" ry="0" width="206.91" height="14" />
		<rect x="401.63" y="68.61" rx="0" ry="0" width="113" height="12" />
		<rect x="401.63" y="85.61" rx="0" ry="0" width="104" height="27" />
		<rect x="401.63" y="84.61" rx="0" ry="0" width="61" height="27" />
		<rect x="401.63" y="129.67" rx="0" ry="0" width="103" height="28" />
		<rect x="401.63" y="163.61" rx="0" ry="0" width="306" height="28" />
		<rect x="401.63" y="192.61" rx="0" ry="0" width="306" height="28" />
		<rect x="401.63" y="223.61" rx="0" ry="0" width="306" height="28" />
		<rect x="401.63" y="254.61" rx="0" ry="0" width="306" height="28" />
		<rect x="401.63" y="284.61" rx="0" ry="0" width="306" height="28" />
		<rect x="401.63" y="314.61" rx="0" ry="0" width="306" height="28" />
	</ContentLoader>
);
